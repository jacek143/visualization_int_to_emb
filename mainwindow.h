#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSerialPort/QtSerialPort>
#include <QQueue>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void updateComboBox(void);
    void openSerialPort(void);
    void readData(void);
    void updateProgressBar(void);
    void updatePlot(void);

private:
    Ui::MainWindow *ui;
    QSerialPort *serial_port;
    const int max_samples = 500;
    const unsigned min_y = 0;
    const unsigned max_y = 255;
    QQueue<uint8_t> samples;
    unsigned total_samples = 0;
    bool triggered = false;
};

#endif // MAINWINDOW_H
