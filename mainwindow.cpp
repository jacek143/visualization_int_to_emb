#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <Qvector>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->serial_port = new QSerialPort(this);

    this->updateComboBox();
    connect(ui->pushButtonRefresh,SIGNAL(clicked(bool)),this,SLOT(updateComboBox()));
    connect(this->serial_port,SIGNAL(readyRead()),this,SLOT(readData()));
    connect(ui->pushButtonOpen,SIGNAL(clicked(bool)),this,SLOT(openSerialPort()));

    ui->plot->addGraph();
    ui->plot->yAxis->setRange(0,this->max_y);
    ui->plot->replot();

    ui->lineEditTrigger->setValidator(new QIntValidator(0,this->max_y,this));
}

MainWindow::~MainWindow()
{
    this->serial_port->close();
    delete ui;
}

void MainWindow::updateComboBox(void){
    ui->comboBox->clear();
    foreach (QSerialPortInfo Info, QSerialPortInfo::availablePorts()) {
        ui->comboBox->addItem(Info.portName() + " - " + Info.description(), Info.portName());
    }
}

void MainWindow::updateProgressBar(){
    if(not samples.isEmpty()){
        ui->progressBar->setValue(samples.head());
    }
}

void MainWindow::openSerialPort(void){
    this->serial_port->close();
    this->serial_port->setPortName(ui->comboBox->currentData().toString());
    this->serial_port->open(QSerialPort::QIODevice::ReadOnly);
}

void MainWindow::readData(void){
    auto new_samples = serial_port->readAll();
    total_samples += new_samples.length();
    bool update_display = false;

    if(not triggered){
        foreach(auto s, new_samples){
            if(ui->pushButtonTrigger->isChecked() and not samples.isEmpty()){
                if(samples.head() >= ui->lineEditTrigger->text().toInt()){
                    triggered = true;
                    ui->pushButtonStop->setChecked(true);
                    ui->pushButtonStop->update();
                    break;
                }
            }
            update_display = true;
            samples.enqueue(s);
            while(samples.length()>max_samples){
                samples.dequeue();
            }
        }
    }

    if(triggered){
        if(not ui->pushButtonStop->isChecked()){
            triggered = false;
            update_display = true;
            samples.clear();
        }
        int i = 0;
        while (samples.length()<max_samples && i < new_samples.length()) {
            update_display = true;
            samples.enqueue(new_samples.at(i++));
        }
    }

    if(update_display){
        updateProgressBar();
        updatePlot();
    }
}

void MainWindow::updatePlot(void){
    QVector<double> y = QVector<double>();
    foreach (auto byte, this->samples) {
        y.append(byte);
    }
    QVector<double> x = QVector<double>(y.length());
    for(int i = 0; i < y.length(); i++){
        x[i] = this->total_samples-y.length()+i;
    }
    ui->plot->graph(0)->setData(x,y);
    ui->plot->xAxis->setRange(x.first(),x.last());
    ui->plot->replot();
}
